import React from 'react';

export default (props)=>{
    const teclaEstilo={
        backgroundColor:'grey',
        color: 'white',
        height: '35px',
        width: '60px',
        margin: '3px',
        textAlign: 'center',
        fontSize: '1.5em',
        borderRadius: '5px'
    }
const teclado = props.valores.map(el=><button onClick={()=>props.clic(el)} style={teclaEstilo}>{el}</button>)
    return(
        <>
        {teclado}
        </>
    )
}