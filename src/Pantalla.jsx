import React from 'react';

export default (props) => {
    const pantallaDiv = {
        height: '40px',
        width: '250px',
        backgroundColor: 'white',
        borderRadius: '5px',
        margin: '5px',
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        fontSize: '2em',
        padding: '5px',
        position: 'relative',

    };

    const operadorPosition = {
        position: 'absolute',
        left: '10px',
        bottom: '5px',
        fontSize: '0.5em'
    };
    const valorAnteriorPosition = {
        position: 'absolute',
        fontSize: '0.5em',
        top: '5px',
        left: '10px'
    }
    return (
        <>
            <div style={pantallaDiv}>
                {props.valor}
                <span style={operadorPosition}>
                    {props.operador}
                </span>
                <span style={valorAnteriorPosition}>
                    {props.valorAnterior}
                </span>
            </div>

        </>
    )
}