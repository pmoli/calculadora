import React, { useState, useEffect } from 'react';
import Pantalla from './Pantalla';
import Teclado from './Teclado';

export default () => {
    const nums = [1, 2, 3, '*', 4, 5, 6, '/', 7, 8, 9, '-', 0, , 'C', '=', '+'];
    const [display, setDisplay] = useState('');
    const [operador, setOperador] = useState()
    const [valorAnterior, setValorAnterior] = useState('')

    const clic = (x) => {
        switch (x) {
            case 'C':
                setDisplay('');
                setValorAnterior('');
                setOperador('')
                break;
            case '+':
                setValorAnterior(display);
                setDisplay('');
                setOperador(x);
                break;
            case '-':
                setValorAnterior(display);
                setDisplay('');
                setOperador(x);
                break;
            case '*':
                setValorAnterior(display);
                setDisplay('');
                setOperador(x);
                break;
            case '/':
                setValorAnterior(display);
                setDisplay('');
                setOperador(x);
                break;
            case '=':
                setDisplay(eval((1*valorAnterior) + operador + (1*display)));
                setValorAnterior('');
                setOperador('')
                break;
            default:
                setDisplay(display + x);
                


        }

    }
    // useEffect(()=>{
    //     setValorAnterior(display);
    //     console.log('vA' + valorAnterior);
    // }, [display])

    const calculadoraEstilo = {
        backgroundColor: 'lightgrey',
        width: '300px',
        height: '250px',
        display: 'flex',
        justifyContent: 'space-around',
        flexWrap: 'wrap',
        alignItems: 'flex-end',
        alignContent: 'flex-end',
        position: 'relative',
        padding: '5px',
        boxShadow: '5px 5px grey',
        borderRadius: '10px'
    };



    return (
        <>
            <div style={calculadoraEstilo}>
                <div style={{ position: 'absolute', top: '10px' }} >
                    <Pantalla valorAnterior={valorAnterior} operador={operador} valor={display} />
                </div>
                <Teclado clic={clic} valores={nums} />
            </div>
        </>
    )

}